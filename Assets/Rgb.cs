﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rgb 
{
    public int red {get; set;}
    public int green {get; set;}
    public int blue {get; set;}
}

