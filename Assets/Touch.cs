﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quobject.SocketIoClientDotNet.Client;
using SocketIO;
using UnityEngine;
using UnityEngine.UI;
using WebSocketSharp;
using Socket = Quobject.SocketIoClientDotNet.Client.Socket;

public class Touch : MonoBehaviour
{
    private Ray _ray;
    private RaycastHit _hit;
    private static Color _color;
    private static Boolean _isTouched;
    private static Boolean _isSending;
    private Socket _socket;
    private static int _r;
    private static int _g;
    private static int _b;

    private Light _lightOne;
    private Light _lightTwo;
    private Light _lightThree;
    private Light _lightFour;
    private Light _lightFive;
    private Light _lightSix;
    private Light _lightSeven;
    private Light _lightEight;
    private Light _lightNine;
    private Light _lightTen;
    private Light _lightEleven;
    private Light _lightTwelve;
    
    private int _red;
    private int _green;
    private int _blue;

    private List<Rgb> listOfRgb = new List<Rgb>(); 
    private List<Light> listOfLights = new List<Light>();

    private Boolean _isDone;

    
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("START");

        _socket = IO.Socket("http://archona-test.site:5001/socha");
        _socket.On(Socket.EVENT_CONNECT, () =>
        {
            Debug.Log("Connected");
            _socket.Emit("state");
        });

        _lightOne = GameObject.Find("OneBotLight").GetComponent<Light>();
        _lightTwo = GameObject.Find("TwoBotLight").GetComponent<Light>();
        _lightThree = GameObject.Find("ThreeBotLight").GetComponent<Light>();
        _lightFour = GameObject.Find("FourBotLight").GetComponent<Light>();
        _lightFive = GameObject.Find("OneMidLight").GetComponent<Light>();
        _lightSix = GameObject.Find("TwoMidLight").GetComponent<Light>();
        _lightSeven = GameObject.Find("ThreeMidLight").GetComponent<Light>();
        _lightEight = GameObject.Find("FourMidLight").GetComponent<Light>();
        _lightNine = GameObject.Find("OneTopLight").GetComponent<Light>();
        _lightTen = GameObject.Find("TwoTopLight").GetComponent<Light>();
        _lightEleven = GameObject.Find("ThreeTopLight").GetComponent<Light>();
        _lightTwelve = GameObject.Find("FourTopLight").GetComponent<Light>();

        listOfLights.Add(_lightOne);
        listOfLights.Add(_lightTwo);
        listOfLights.Add(_lightThree);
        listOfLights.Add(_lightFour);
        listOfLights.Add(_lightFive);
        listOfLights.Add(_lightSix);
        listOfLights.Add(_lightSeven);
        listOfLights.Add(_lightEight);
        listOfLights.Add(_lightNine);
        listOfLights.Add(_lightTen);
        listOfLights.Add(_lightEleven);
        listOfLights.Add(_lightTwelve);

        _socket.On("led_response", (data) =>
        {
            _isSending = false;
            _isDone = false;
            var k = data as JObject;

            Debug.Log(k.GetValue("data"));

            for (int i = 0; i < 2; i++)
            {

                var kk = k.GetValue("data")[i].ToString();

                _red = (int) JObject.Parse(kk)["red"];
                _green = (int) JObject.Parse(kk)["green"];
                _blue = (int) JObject.Parse(kk)["blue"];

                Debug.Log(_red);
                Debug.Log(_green);
                Debug.Log(_blue);

                var rgb = new Rgb()
                {
                    red = _red,
                    blue = _blue,
                    green = _green
                };

                listOfRgb.Add(rgb);
            }

            _isDone = true;
        });

        Image image = GameObject.FindWithTag("SlotOne").GetComponent<Image>();
        image.color = Color.blue;

        image = GameObject.FindWithTag("SlotTwo").GetComponent<Image>();
        image.color = Color.cyan;

        image = GameObject.FindWithTag("SlotThree").GetComponent<Image>();
        image.color = Color.magenta;

        image = GameObject.FindWithTag("SlotFour").GetComponent<Image>();
        image.color = Color.red;

        image = GameObject.FindWithTag("SlotFive").GetComponent<Image>();
        image.color = Color.green;

        image = GameObject.FindWithTag("SlotSix").GetComponent<Image>();
        image.color = Color.yellow;

        var colorBlock = new ColorBlock
        {
            colorMultiplier = 1, normalColor = Color.white, fadeDuration = 0.01f, pressedColor = Color.white,
            selectedColor = Color.black, highlightedColor = Color.black, disabledColor = Color.blue
        };
        Button button = GameObject.FindWithTag("BorderOne").GetComponent<Button>();
        button.colors = colorBlock;

        colorBlock = new ColorBlock
        {
            colorMultiplier = 1, normalColor = Color.white, fadeDuration = 0.01f, pressedColor = Color.white,
            selectedColor = Color.black, highlightedColor = Color.black, disabledColor = Color.cyan
        };
        button = GameObject.FindWithTag("BorderTwo").GetComponent<Button>();
        button.colors = colorBlock;

        colorBlock = new ColorBlock
        {
            colorMultiplier = 1, normalColor = Color.white, fadeDuration = 0.01f, pressedColor = Color.white,
            selectedColor = Color.black, highlightedColor = Color.black, disabledColor = Color.magenta
        };
        button = GameObject.FindWithTag("BorderThree").GetComponent<Button>();
        button.colors = colorBlock;

        colorBlock = new ColorBlock
        {
            colorMultiplier = 1, normalColor = Color.white, fadeDuration = 0.01f, pressedColor = Color.white,
            selectedColor = Color.black, highlightedColor = Color.black, disabledColor = Color.red
        };
        button = GameObject.FindWithTag("BorderFour").GetComponent<Button>();
        button.colors = colorBlock;

        colorBlock = new ColorBlock
        {
            colorMultiplier = 1, normalColor = Color.white, fadeDuration = 0.01f, pressedColor = Color.white,
            selectedColor = Color.black, highlightedColor = Color.black, disabledColor = Color.green
        };
        button = GameObject.FindWithTag("BorderFive").GetComponent<Button>();
        button.colors = colorBlock;

        colorBlock = new ColorBlock
        {
            colorMultiplier = 1, normalColor = Color.white, fadeDuration = 0.01f, pressedColor = Color.white,
            selectedColor = Color.black, highlightedColor = Color.black, disabledColor = Color.yellow
        };
        button = GameObject.FindWithTag("BorderSix").GetComponent<Button>();
        button.colors = colorBlock;

        colorBlock = new ColorBlock {colorMultiplier = 1, normalColor = Color.white};
        button = GameObject.FindWithTag("NextColors").GetComponent<Button>();
        button.colors = colorBlock;
        
        for (int j = 0; j < listOfRgb.Count; j++)
        {
            var rgbFromList = listOfRgb[j];
            var count = rgbFromList.red + rgbFromList.blue + rgbFromList.green;

            if (count == 0)
            {
                Debug.Log("null");
                listOfLights[j].enabled = false;
            }
            else
            {
                listOfLights[j].color = new Color(rgbFromList.red, rgbFromList.green, rgbFromList.blue, 1.0f);
            }
        }

    }

    /*private void Callback(data)
    {
        Debug.LogFormat();
    }*/

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 1)
        {
            _ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            
            if(Physics.Raycast(_ray, out _hit, Mathf.Infinity) && Input.GetTouch(0).phase != TouchPhase.Moved)
            {
                
                Debug.Log("Hit");
                GameObject touchedObject = _hit.transform.gameObject;
                
                Debug.Log(touchedObject.name);

                if (_isTouched)
                {
                    switch (touchedObject.name)
                    {
                        case "FourBot":
                            _lightFour.enabled = true;
                            _lightFour.color = _color;
                            Led(4);
                            break;
                        case "ThreeBot":
                            _lightThree.enabled = true;
                            _lightThree.color = _color;
                            Led(3);
                            break;
                        case "TwoBot":
                            _lightTwo.enabled = true;
                            _lightTwo.color = _color;
                            Led(2);
                            break;
                        case "OneBot":
                            _lightOne.enabled = true;
                            _lightOne.color = _color;
                            Led(1);
                            break;
                        case "FourMid":
                            _lightEight.enabled = true;
                            _lightEight.color = _color;
                            Led(8);
                            break;
                        case "ThreeMid":
                            _lightSeven.enabled = true;
                            _lightSeven.color = _color;
                            Led(7);
                            break;
                        case "TwoMid":
                            _lightSix.enabled = true;
                            _lightSix.color = _color;
                            Led(6);
                            break;
                        case "OneMid":
                            _lightFive.enabled = true;
                            _lightFive.color = _color;
                            Led(5);
                            break;
                        case "FourTop":
                            _lightTwelve.enabled = true;
                            _lightTwelve.color = _color;
                            Led(12);
                            break;
                        case "ThreeTop":
                            _lightEleven.enabled = true;
                            _lightEleven.color = _color;
                            Led(11);
                            break;
                        case "TwoTop":
                            _lightTen.enabled = true;
                            _lightTen.color = _color;
                            Led(10);
                            break;
                        case "OneTop":
                            _lightNine.enabled = true;
                            _lightNine.color = _color;
                            Led(9);
                            break;
                        default:
                            Debug.Log("Nothing happend");
                            break;
                    }
                }
            }
        }
    }

    private void Led(int i)
    {

        if (_isSending)
        {
            return;
        }

        _isSending = true;
        
        Led json = new Led() {
            
            index = i,
            rgb = new Rgb() {
                red = _r,
                green = _g,
                blue =_b,
            }
        };

        var serializedJson = JsonConvert.SerializeObject(json);
        
        Debug.Log(serializedJson);
        
        _socket.Emit("led", serializedJson);

    }

    public static void SetColor(Color color)
    {
        _color = color;
        _r = Convert.ToInt32(_color.r * 255);
        _g = Convert.ToInt32(_color.g * 255);
        _b = Convert.ToInt32(_color.b * 255);
        _isTouched = true;
    }

    Color GetColor()
    {
        return _color;
    }
}
