﻿using UnityEngine;

public class ObjectRotation : MonoBehaviour
{

    private UnityEngine.Touch _touch;
    private Vector2 _touchPosition;
    private Quaternion _rotationY;
    private float _speedModifier = 0.15f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.touchCount > 0)
        {
            _touch = Input.GetTouch(0);

            if (_touch.phase == TouchPhase.Moved)
            {
                _rotationY = Quaternion.Euler(0f, - _touch.deltaPosition.x * _speedModifier, 0f);

                var transform1 = transform;
                transform1.rotation = _rotationY * transform1.rotation;
            }
        }
        
    }
}
