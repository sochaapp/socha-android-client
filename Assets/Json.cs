﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Led 
{
    public int index { get; set; }
    public Rgb rgb { get; set; }
}
