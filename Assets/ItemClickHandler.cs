﻿using UnityEngine;
using UnityEngine.UI;

public class ItemClickHandler : MonoBehaviour
{
    public void OnItemClicked()
    {
        Button button = GetComponent<Button>();
        Color color = button.colors.disabledColor;
        Debug.Log(color.ToString());

        Touch.SetColor(color);
    }
}
